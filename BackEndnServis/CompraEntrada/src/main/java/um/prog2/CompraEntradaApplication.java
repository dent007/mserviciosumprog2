package um.prog2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
@EnableDiscoveryClient // Anotacioon que regitra este proyecto en Eureka
@EnableResourceServer//pq es un proy que maneja recursos -los tocken 
@SpringBootApplication
public class CompraEntradaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompraEntradaApplication.class, args);
	}

}
