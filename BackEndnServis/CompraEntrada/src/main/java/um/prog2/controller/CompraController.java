package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.dto.UserClienteDto;
import um.prog2.model.Cliente;
import um.prog2.service.IClienteService;

@RestController 
@RequestMapping("/compra") 
public class CompraController {
	
	@Autowired
	private IClienteService clienteservicio;
	
	@PostMapping( value = "/cliente",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer>  obtenerClienteCompra(@RequestBody  UserClienteDto userLoginDto) {
		System.out.println(userLoginDto.toString());
		//UserClienteDto {String userLoing ,String rolLoin}
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		int clienteID=0;
		//Cliente cliente = new Cliente();
		try {
			
			clienteID=clienteservicio.clientOnline(userLoginDto);
			//cliente=clienteservicio.buscarClienteUsuario(userLoginDto);
			
			
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Integer>(clienteID, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(clienteID, HttpStatus.OK);
	}


}
