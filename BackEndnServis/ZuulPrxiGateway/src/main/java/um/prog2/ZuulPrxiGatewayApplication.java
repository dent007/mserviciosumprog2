package um.prog2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
@EnableDiscoveryClient // aparece en dashboard
@EnableZuulProxy // habilita tabla de ruteo, app permita reconocer a us via un apodo; este modulo solo redirije, no tiene cuerpo
@SpringBootApplication
public class ZuulPrxiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulPrxiGatewayApplication.class, args);
		System.out.println("ZUUL BIEN!!!");

	}

}
