package um.prog2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Autowired
    private ResourceServerTokenServices tokenServices;
	
    @Value("${security.jwt.resource-ids}")
    private String resourceIds;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(resourceIds).tokenServices(tokenServices);
    }
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
                http
                .requestMatchers()
                .and()
                .authorizeRequests()
                //.antMatchers("/cliente/**" ).authenticated()
                .antMatchers("/cliente/listarPageable/**" ).authenticated()
                .antMatchers("/cliente/listar/**" ).authenticated()/*listar y listar/{id}*/
                .antMatchers("/cliente/eliminar/**" ).authenticated()
                .antMatchers("/cliente/actualizar/**" ).authenticated()
                
                .antMatchers("/cliente/registrar/**" ).permitAll()
                
                .antMatchers("/butaca/**" ).authenticated()
                .antMatchers("/calificacion/**" ).authenticated()
                .antMatchers("/entrada/**" ).authenticated()
                .antMatchers("/funcion/**" ).authenticated()
                .antMatchers("/ocupacion/**" ).authenticated()
                .antMatchers("/pelicula/**" ).authenticated()
                .antMatchers("/sala/**" ).authenticated()
                .antMatchers("/compra/**" ).authenticated()
                .antMatchers("/pago/**" ).authenticated()
                .antMatchers("/targeta/**" ).authenticated()
                .antMatchers("/ticket/**" ).authenticated(); 
                
                
                
    }
}
