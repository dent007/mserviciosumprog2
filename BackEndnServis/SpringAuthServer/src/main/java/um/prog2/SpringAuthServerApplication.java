package um.prog2;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableDiscoveryClient // Anotacioon que regitra este proyecto en Eureka
@RestController
@SpringBootApplication
public class SpringAuthServerApplication {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


		public static void main(String[] args) {
			SpringApplication.run(SpringAuthServerApplication.class, args);
			System.out.println("OAUTH BIEN!!!");
		
		}
		
		@Autowired
		private OAuth2ClientContext context;
		
		@GetMapping("/access_token")
		public String getToken() {
			String token = context.getAccessToken().getValue();
			System.out.println("Token: " + token);
			return token;
		}
		
		@GetMapping("/user")
		public Principal getUser(Principal user) {
			  // logger.debug("Sample debug message");              
			   logger.info("Sample info message");
			   //logger.warn("Sample warn message");
			   //logger.error("Sample error message");
			return user;
		}
	}