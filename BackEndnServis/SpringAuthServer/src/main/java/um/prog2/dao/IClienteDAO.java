package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import um.prog2.model.Cliente;
@Repository
public interface IClienteDAO extends JpaRepository <Cliente, Integer>{
	
	Cliente findByUsuarioAuthorityAndUsuarioUsername(String rolAutoity, String username);	
	
	@Query("FROM Cliente cl WHERE cl.usuario.authority=:rolAutoity AND  cl.usuario.username=:user")
	  Cliente clientOnline (@Param("user") String user,@Param("rolAutoity") String rolAutoity);

}
