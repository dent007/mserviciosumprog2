package um.prog2.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import um.prog2.dto.UserClienteDto;
import um.prog2.model.Cliente;
import um.prog2.model.Usuario;

public interface IClienteService {
	/*Retorna el id del últimmo objeto registrado*/
	//int registrar(Usuario us);
	int registrar(Cliente cl);
	int modificar(Cliente but);
	void eliminar(int idCli);
	Cliente listarId(int idCli) ;
	List<Cliente> listar(); 
	Page<Cliente> listAllbyPage(Pageable pageable);
	
	int clientOnline (UserClienteDto userLogin);
	Cliente buscarClienteUsuario(UserClienteDto userLogin);

	/*Cliente clientOnline (String user, String rolAutoity);
	Cliente findByUsuarioAuthorityAndUsuarioUsername(String user, String rolAutoity);*/
}
