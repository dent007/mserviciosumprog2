package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.prog2.model.Usuario;

@Repository
public interface IUsuarioDAO extends JpaRepository<Usuario, Integer> {
	
	Usuario findOneByUsername(String username);
//	Usuario findByUsernameAndAuthority(String user, String rolAutoity);
}
