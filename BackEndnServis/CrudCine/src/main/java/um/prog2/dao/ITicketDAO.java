package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.prog2.model.Ticket;
@Repository
public interface ITicketDAO extends JpaRepository <Ticket, Integer>{

}
