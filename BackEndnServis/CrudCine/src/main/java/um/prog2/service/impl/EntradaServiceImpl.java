package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.IEntradaDAO;
import um.prog2.model.Entrada;
import um.prog2.service.IEntradaServicio;

@Service
public class EntradaServiceImpl implements IEntradaServicio{
	
	@Autowired
	private IEntradaDAO entradadao;
	@Override
	public int registrar(Entrada ent) {
		
		int respuesta=0;		
		respuesta = entradadao.save(ent) !=null ? ent.getEntrada_id():0;
		return respuesta > 0 ? ent.getEntrada_id() : 0;
	}

	@Override
	public int modificar(Entrada ent) {
		int respuesta=0;		
		respuesta = entradadao.save(ent) !=null ? ent.getEntrada_id():0;
		return respuesta > 0 ? 1 : 0;
	}

	@Override
	public void eliminar(int idEnt) {
		entradadao.delete(idEnt);
	}

	@Override
	public Entrada listarId(int idEnt) {
		return entradadao.findOne(idEnt);
	}

	@Override
	public List<Entrada> listar() {
		return entradadao.findAll();
	}

}
