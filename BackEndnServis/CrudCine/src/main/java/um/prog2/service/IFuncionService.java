package um.prog2.service;

import java.util.List;

import um.prog2.model.Funcion;

public interface IFuncionService {
	/*Retorna el id del últimmo objeto registrado*/
	int registrar(Funcion fun);
	int modificar(Funcion fun);
	void eliminar(int idFun);
	Funcion listarId(int idFun) ;
	List<Funcion> listar(); 
}
