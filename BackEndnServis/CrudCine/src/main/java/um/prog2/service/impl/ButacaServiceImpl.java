package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.IButacaDAO;
import um.prog2.model.Butaca;
import um.prog2.service.IButacaService;
@Service
public class ButacaServiceImpl implements IButacaService{
	
	@Autowired
	private IButacaDAO butacadao;
	
	@Override
	public int registrar(Butaca but) {
		/*int retorno = 0;		
		retorno= butacadao.save(but)!=null ? but.getButaca_id():0;
		return retorno >0 ?1:0;*/
		int retorno = 0,idB=0;		
		retorno= butacadao.save(but)!=null ? (idB=but.getButaca_id()):0;
		//return retorno >0 ?1:0;
		return retorno >0 ?idB:0;
	}

	@Override
	public int modificar(Butaca but) {
		int retorno = 0;		
		retorno= butacadao.save(but)!=null ? but.getButaca_id():0;
		return retorno >0 ?1:0;
		
	}

	@Override
	public void eliminar(int idBut) {
		butacadao.delete(idBut);
	}

	@Override
	public Butaca listarId(int idBut) {
		return butacadao.findOne(idBut);
	}

	@Override
	public List<Butaca> listar() {
		return butacadao.findAll();
	}

}
