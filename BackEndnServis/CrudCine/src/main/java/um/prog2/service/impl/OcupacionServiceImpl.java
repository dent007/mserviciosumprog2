package um.prog2.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.IButacaDAO;
import um.prog2.dao.IFuncionDAO;
import um.prog2.dao.IOcupacionDAO;
import um.prog2.dto.CompraDTO;
import um.prog2.model.Butaca;
import um.prog2.model.Funcion;
import um.prog2.model.Ocupacion;
import um.prog2.service.IOcupacionService;
@Service
public class OcupacionServiceImpl implements IOcupacionService{
	@Autowired
	private IOcupacionDAO ocupaciondao;
	
	@Autowired
	private IFuncionDAO funciondao;
	
	@Autowired
	private IButacaDAO butacadao;
	
	@Override
	public int registrar(Ocupacion ocu) {
		int retorno=0;
		retorno=ocupaciondao.save(ocu) !=null?ocu.getOcupacion_id():0;
		return retorno > 0 ? 1 : 0;
	}

	@Override
	public int modificar(Ocupacion ocu) {
		int retorno = 0;
		retorno=ocupaciondao.save(ocu) !=null ? ocu.getOcupacion_id():0;
		return retorno > 0 ? 1 : 0;
	}

	@Override
	public void eliminar(int idOcu) {
		ocupaciondao.delete(idOcu);
	}

	@Override
	public Ocupacion listarId(int idOcu) {
		return ocupaciondao.findOne(idOcu);
	}

	@Override
	public List<Ocupacion> listar() {
		return ocupaciondao.findAll();
	}

	@Override
	public List<CompraDTO> listarPrecompraEntrada() {
		
		List<CompraDTO> comprasDTO= new ArrayList<>();
		
		List<Ocupacion> ocs = ocupaciondao.findAll();
		for(Ocupacion o : ocs) {
		CompraDTO compra= new CompraDTO();	
			compra.setButacas(o.getButaca());
			compra.setFuncion(o.getFuncion());
			comprasDTO.add(compra);
		}
		
	// TODO Auto-generated method stub
		return comprasDTO;
	}

}
