package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.IPeliculaDAO;
import um.prog2.model.Pelicula;
import um.prog2.service.IPeliculaService;
@Service
public class PeliculaServiceImpl implements IPeliculaService{

	@Autowired
	private IPeliculaDAO peliculadao;

	@Override
	public int registrar(Pelicula pel) {
		int retorno =0;
		retorno=peliculadao.save(pel)!=null ? pel.getPelicula_id():0;
		return retorno > 0 ? 1 : 0;
	}

	@Override
	public int modificar(Pelicula pel) {
		int retorno =0;
		retorno=peliculadao.save(pel)!=null ? pel.getPelicula_id():0;
		return retorno > 0 ? 1 : 0;	}

	@Override
	public void eliminar(int idPel) {
		peliculadao.delete(idPel);
	}

	@Override
	public Pelicula listarId(int idPel) {
		return peliculadao.findOne(idPel);
	}

	@Override
	public List<Pelicula> listar() {
		return peliculadao.findAll();
	}

}
