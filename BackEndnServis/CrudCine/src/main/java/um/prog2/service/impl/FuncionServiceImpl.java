package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.IFuncionDAO;
import um.prog2.model.Funcion;
import um.prog2.service.IFuncionService;
@Service
public class FuncionServiceImpl implements IFuncionService{
	@Autowired
	private IFuncionDAO funciondao;
	
	@Override
	public int registrar(Funcion fun) {
		int retorno =0;
		retorno= funciondao.save(fun)!=null?fun.getFuncion_id():0;
		return  retorno >0 ? 1:0;
	}

	@Override
	public int modificar(Funcion fun) {
		int retorno =0;
		retorno= funciondao.save(fun)!=null?fun.getFuncion_id():0;
		return  retorno >0 ? 1:0;
	}

	@Override
	public void eliminar(int idFun) {
		funciondao.delete(idFun);
	}

	@Override
	public Funcion listarId(int idFun) {
		return funciondao.findOne(idFun);
	}

	@Override
	public List<Funcion> listar() {
		return funciondao.findAll();
	}

}
