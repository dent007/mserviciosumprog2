package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.ISalaDAO;
import um.prog2.model.Sala;
import um.prog2.service.ISalaService;
@Service
public class SalaServiceImpl implements ISalaService{
	 @Autowired
	 private ISalaDAO saladao;

	@Override
	public int registrar(Sala sal) {
		
		int respuesta=0;		
		respuesta = saladao.save(sal) !=null ? sal.getSala_id():0;
		return respuesta > 0 ? 1 : 0;
	}

	@Override
	public int modificar(Sala sal) {	
		int respuesta=0;		
		respuesta = saladao.save(sal) !=null ? sal.getSala_id():0;
		return respuesta > 0 ? 1 : 0;
	}

	@Override
	public void eliminar(int idSal) {
		saladao.delete(idSal);
	}

	@Override
	public Sala listarId(int idSal) {
		return saladao.findOne(idSal);
	}

	@Override
	public List<Sala> listar() {
		return saladao.findAll();
	}

}
