package um.prog2.service;

import java.util.List;

import um.prog2.model.Calificacion;

public interface ICalificacionService {
	/*Retorna el id del últimmo objeto registrado*/
	int registrar(Calificacion cal);
	int modificar(Calificacion cal);
	void eliminar(int idCal);
	Calificacion listarId(int idCal) ;
	List<Calificacion> listar(); 
}
