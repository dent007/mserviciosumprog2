package um.prog2.service;

import java.util.List;

import um.prog2.model.Entrada;

public interface IEntradaServicio {
	/*Retorna el id del últimmo objeto registrado*/
	int registrar(Entrada ent);
	int modificar(Entrada ent);
	void eliminar(int idEnt);
	Entrada listarId(int idEnt) ;
	List<Entrada> listar(); 
}
