package um.prog2.service;

import java.util.List;

import um.prog2.model.Pelicula;

public interface IPeliculaService {
	/*Retorna el id del últimmo objeto registrado*/
	int registrar(Pelicula pel);
	int modificar(Pelicula pel);
	void eliminar(int idPel);
	Pelicula listarId(int idPel) ;
	List<Pelicula> listar(); 

}
