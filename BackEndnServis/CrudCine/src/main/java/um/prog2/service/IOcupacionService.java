package um.prog2.service;

import java.util.List;

import um.prog2.dto.CompraDTO;

import um.prog2.model.Ocupacion;

public interface IOcupacionService {
	/*Retorna el id del últimmo objeto registrado*/
	int registrar(Ocupacion ocu);
	int modificar(Ocupacion ocu);
	void eliminar(int idOcu);
	Ocupacion listarId(int idOcu) ;
	List<Ocupacion> listar(); 
	List<CompraDTO> listarPrecompraEntrada(); 
}
