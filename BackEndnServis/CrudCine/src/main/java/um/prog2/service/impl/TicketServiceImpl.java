package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.ITicketDAO;
import um.prog2.model.Ticket;
import um.prog2.service.ITicketService;
@Service
public class TicketServiceImpl implements ITicketService{
	
	@Autowired
	private ITicketDAO ticketdao;
	
	@Override
	public int registrar(Ticket tik) {
		int respuesta=0;
		respuesta = ticketdao.save(tik) !=null ? tik.getTicket_id():0;
		return respuesta > 0 ? tik.getTicket_id() : 0;
	}

	@Override
	public int modificar(Ticket tik) {
		int respuesta=0;
		respuesta = ticketdao.save(tik) !=null ? tik.getTicket_id():0;
		return respuesta > 0 ? 1 : 0 ;
		
	}

	@Override
	public void eliminar(int idTik) {
		ticketdao.delete(idTik);
		
	}

	@Override
	public Ticket listarId(int idTik) {
		return ticketdao.findOne(idTik);
	}

	@Override
	public List<Ticket> listar() {		
		return ticketdao.findAll();
	}

}
