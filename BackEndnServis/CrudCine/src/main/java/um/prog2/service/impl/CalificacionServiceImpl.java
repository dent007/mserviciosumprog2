package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.ICalificacionDAO;
import um.prog2.model.Calificacion;
import um.prog2.service.ICalificacionService;

@Service
public class CalificacionServiceImpl implements ICalificacionService{
	
	@Autowired
	private ICalificacionDAO calificacionDao;
	
	@Override
	public int registrar(Calificacion cal) {
		int retorno=0;
		retorno = calificacionDao.save(cal)!=null ? cal.getCalificacion_id():0;
		return retorno > 0 ? 1:0;
	}

	@Override
	public int modificar(Calificacion cal) {
		int retorno=0;
		retorno = calificacionDao.save(cal)!=null ? cal.getCalificacion_id():0;
		return retorno > 0 ? 1:0;
		// JPA;Si encuentra id nuevo que no está en BD => insertará, pero si ya existe ese id lo actualizará
	}
	
	@Override
	public void eliminar(int idCal) {
		
		//calificacionDao.delete(idCal);
		calificacionDao.delete(idCal);

	}
	
	@Override
	public Calificacion listarId(int idCal) {
		return calificacionDao.findOne(idCal);
	}

	@Override
	public List<Calificacion> listar() {
		//System.out.println("salida");
		return calificacionDao.findAll(); 
		
	}



}
