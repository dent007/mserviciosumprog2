package um.prog2.service;

import java.util.List;

import um.prog2.model.Sala;

public interface ISalaService {
	/*Retorna el id del últimmo objeto registrado*/
	int registrar(Sala sal);
	int modificar(Sala sal);
	void eliminar(int idSal);
	Sala listarId(int idSal) ;
	List<Sala> listar(); 
}
