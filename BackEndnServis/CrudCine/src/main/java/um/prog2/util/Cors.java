package um.prog2.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
@Component // El CORDS habilita a q estos servicios rest sean consumidos de otro dominio, no solo localhost:8080. localhost:2400
@Order(Ordered.HIGHEST_PRECEDENCE)
public class Cors implements Filter{
	
	@Override
	public void init(FilterConfig filterConfig)throws ServletException{
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		
			HttpServletResponse response = (HttpServletResponse) res;
			HttpServletRequest request = (HttpServletRequest) req;
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
			response.setHeader("Access-Control-Max-Age", "3600");
			
			response.setHeader("Access-Control-Allow-Headers", "x-requested-with, authorization,Authorization, Content-Type,credential,X-XSRF-TOKEN");																			
			response.addHeader("Access-Control-Expose-Headers", "xsrf-token");
	        // For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS handshake
			
	        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
	            response.setStatus(HttpServletResponse.SC_OK);
	            
	        }else {
	        	chain.doFilter(req,res);	
	        }
	 
	        // pass the request along the filter chain
	        
		
	}

	@Override
	public void destroy() {
		// TO-DO Auto-generated method stub
		
	}
}
