package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Ticket;
import um.prog2.service.ITicketService;

@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/ticket") // Indica una ruta de acceso url
public class TicketController {
	@Autowired//El objeto existe si o si, ¿se usará y dará val a sus atributos?
	private ITicketService ticketService;
	

	
	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Ticket>> listarTicketsTodos() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Ticket> ticket = new ArrayList<>();
		try {
			ticket=ticketService.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Ticket>>(ticket, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Ticket>>(ticket, HttpStatus.OK);
	}
	
	@GetMapping( value = "/listar/{idTik}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Ticket> listarSalaPorId(@PathVariable("idTik") Integer idTik) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Ticket ticket = new Ticket();
		try {
			ticket=ticketService.listarId(idTik);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Ticket>(ticket, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Ticket>(ticket, HttpStatus.OK);
	}	
	
	@DeleteMapping( value = "/eliminar/{idTik}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarTicketPorId(@PathVariable("idTik") Integer idTik) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			ticketService.eliminar(idTik);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarTicket(@RequestBody Ticket tik) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Ticket ticket = new Ticket(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=ticketService.registrar(tik); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	
	
	  @PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<Integer> modificarTicket(@RequestBody Ticket tik){
	    int retorno =0;
	    try {
	    	retorno=ticketService.modificar(tik);
	    }catch (Exception e){
	    	return new ResponseEntity< Integer>(retorno,HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	    return new ResponseEntity< Integer>(retorno,HttpStatus.OK);
	  }
}
