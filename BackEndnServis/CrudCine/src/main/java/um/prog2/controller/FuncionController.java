package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Funcion;
import um.prog2.service.IFuncionService;

@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/funcion") // Indica una ruta de acceso url
public class FuncionController {
	
	@Autowired
	private IFuncionService funcionService;
	


	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Funcion>> listarFuncionesTodas() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Funcion> funcion = new ArrayList<>();
		try {
			funcion=funcionService.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Funcion>>(funcion, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Funcion>>(funcion, HttpStatus.OK);
	}
	
	@GetMapping( value = "/listar/{idFun}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Funcion> listarFuncionPorId(@PathVariable("idFun") Integer idFun) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Funcion funcion = new Funcion();
		try {
			funcion=funcionService.listarId(idFun);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Funcion>(funcion, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Funcion>(funcion, HttpStatus.OK);
	}
	
	@DeleteMapping( value = "/eliminar/{idFun}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarFuncionPorId(@PathVariable("idFun") Integer idFun) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			funcionService.eliminar(idFun);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarPelicula(@RequestBody Funcion fun) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Funcion funcion = new Funcion(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno =0;
		try {
			retorno=funcionService.registrar(fun); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}	

@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Integer> modificarPelicula(@RequestBody Funcion fun) {
	
	//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
	//Funcion funcion = new Funcion(); //if(id)no hubo registro-> algo  else{si -> algo}
	int retorno =0;
	try {
		retorno=funcionService.modificar(fun); // retorna id 
	} catch (Exception e) {
		//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
		return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
	}	
	return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
}
}
