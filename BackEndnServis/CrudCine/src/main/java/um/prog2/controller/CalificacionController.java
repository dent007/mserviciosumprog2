package um.prog2.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Calificacion;
import um.prog2.service.ICalificacionService;

/*
 * Capa que   expone el servicio para ser consumido
 */

@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/calificacion") // Indica una ruta de acceso
public class CalificacionController {

	@Autowired//El objeto existe si o si, ¿se usará y dará val a sus atributos?
	private ICalificacionService califServicio;
	
	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)  
	public ResponseEntity<List<Calificacion>> listarCalificacionesTodas() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Calificacion> calificacions = new ArrayList<>();
		try {
			calificacions=califServicio.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Calificacion>>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Calificacion>>(calificacions, HttpStatus.OK);
	}
	
	@GetMapping( value = "/listar/{idCal}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Calificacion> listarCalificacionPorId(@PathVariable("idCal") Integer idCal) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Calificacion calificacions = new Calificacion();
		try {
			calificacions=califServicio.listarId(idCal);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Calificacion>(calificacions, HttpStatus.OK);
	}
	
	@DeleteMapping( value = "/eliminar/{idCal}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarCalificacionPorId(@PathVariable("idCal") Integer idCal) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			califServicio.eliminar(idCal);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarCalificacion(@RequestBody Calificacion calif) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=califServicio.registrar(calif); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	
	  @PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity< Integer > modificarCalificacion(@RequestBody Calificacion calificacions){
	    int resultado =0;
	    try {
	    	resultado= califServicio.modificar(calificacions);
	     // resultado=1;
	    }catch (Exception e){
	     // resultado=0;
		    return new ResponseEntity< Integer>(resultado,HttpStatus.OK);

	    }
	    return new ResponseEntity< Integer>(resultado,HttpStatus.OK);
	  }

}
