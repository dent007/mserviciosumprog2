package um.prog2.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Sala;
import um.prog2.service.ISalaService;

@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/sala") // Indica una ruta de acceso url
public class SalaController {
		
		@Autowired
		private ISalaService salaService;
		
		@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<List<Sala>> listarSalasTodos() {
			
			//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
			List<Sala> sala = new ArrayList<>();
			try {
				sala=salaService.listar();
			} catch (Exception e) {
				//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
				return new ResponseEntity<List<Sala>>(sala, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
			return new ResponseEntity<List<Sala>>(sala, HttpStatus.OK);
		}

		@GetMapping( value = "/listar/{idSal}", produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Sala> listarSalaPorId(@PathVariable("idSal") Integer idSal) {
			
			//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
			Sala sala = new Sala();
			try {
				sala=salaService.listarId(idSal);
			} catch (Exception e) {
				//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
				return new ResponseEntity<Sala>(sala, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
			return new ResponseEntity<Sala>(sala, HttpStatus.OK);
		}
		
		@DeleteMapping( value = "/eliminar/{idSal}", produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Integer> eliminarSalaPorId(@PathVariable("idSal") Integer idSal) {
			
			//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
			//Calificacion calificacions = new Calificacion();
			int resultado=0;
			try {
				//calificacions=califServicio.eliminar(idCal);
				salaService.eliminar(idSal);
				resultado =1;
			} catch (Exception e) {
				//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
				//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
				resultado=0;
			}	
			return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
		}
		@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Integer> registrarSala(@RequestBody Sala sal) {
			
			//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
			//Sala sala = new Sala(); //if(id)no hubo registro-> algo  else{si -> algo}
			int retorno=0;
			try {
				retorno=salaService.registrar(sal); // retorna id 
			} catch (Exception e) {
				//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
				return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
			return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
		}
		  @PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity< Integer > modificarSala(@RequestBody Sala sal){
		    int retorno =0;
		    try {
		    	retorno=salaService.modificar(sal);
		    }catch (Exception e){
				return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		    return new ResponseEntity< Integer>(retorno,HttpStatus.OK);
		  }
		
	}
