package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Pelicula;
import um.prog2.service.IPeliculaService;

@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/pelicula") // Indica una ruta de acceso url
public class PeliculaController {

@Autowired
private IPeliculaService peliculaService;



@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<List<Pelicula>> listarPeliculasTodas() {
	
	//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
	List<Pelicula> pelicula = new ArrayList<>();
	try {
		pelicula=peliculaService.listar();
	} catch (Exception e) {
		//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
		return new ResponseEntity<List<Pelicula>>(pelicula, HttpStatus.INTERNAL_SERVER_ERROR);
	}	
	return new ResponseEntity<List<Pelicula>>(pelicula, HttpStatus.OK);
}

@GetMapping( value = "/listar/{idPel}", produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Pelicula> listarPeliculaPorId(@PathVariable("idPel") Integer idPel) {
	
	//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
	Pelicula pelicula = new Pelicula();
	try {
		pelicula=peliculaService.listarId(idPel);
	} catch (Exception e) {
		//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
		return new ResponseEntity<Pelicula>(pelicula, HttpStatus.INTERNAL_SERVER_ERROR);
	}	
	return new ResponseEntity<Pelicula>(pelicula, HttpStatus.OK);
}
@DeleteMapping( value = "/eliminar/{idPel}", produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Integer> eliminarPeliculaPorId(@PathVariable("idPel") Integer idPel) {
	
	//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
	//Calificacion calificacions = new Calificacion();
	int resultado=0;
	try {
		//calificacions=califServicio.eliminar(idCal);
		peliculaService.eliminar(idPel);
		resultado =1;
	} catch (Exception e) {
		//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
		//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
		resultado=0;
	}	
	return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
}
@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Integer> registrarPelicula(@RequestBody Pelicula pel) {
	
	//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
	//Pelicula pelicula = new Pelicula(); //if(id)no hubo registro-> algo  else{si -> algo}
	int retorno=0;
	try {
		retorno=peliculaService.registrar(pel); // retorna id 
	} catch (Exception e) {
		//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
		return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
	}	
	return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
}
@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<Integer> modificarPelicula(@RequestBody Pelicula pel) {
	
	//Pelicula pelicula = new Pelicula(); //if(id)no hubo registro-> algo  else{si -> algo}
	int retorno=0;
	try {
		retorno=peliculaService.modificar(pel); // retorna id 
	} catch (Exception e) {
		//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
		return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
	}	
	return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
}
}
