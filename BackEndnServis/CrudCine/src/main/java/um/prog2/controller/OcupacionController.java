package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.dto.CompraDTO;
import um.prog2.model.Ocupacion;
import um.prog2.service.IOcupacionService;

@RestController // Esteriotipa la clase como un controlador
@RequestMapping("/ocupacion") // Indica una ruta de acceso url
public class OcupacionController {
	
	@Autowired
	private IOcupacionService ocupacionServicio;
	

	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Ocupacion>> listarOcupacionesTodas() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Ocupacion> ocupacion = new ArrayList<>();
		try {
			ocupacion=ocupacionServicio.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Ocupacion>>(ocupacion, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Ocupacion>>(ocupacion, HttpStatus.OK);
	}
	
	@GetMapping( value = "/listar/{idOcu}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Ocupacion> listarOcupacionPorId(@PathVariable("idOcu") Integer idOcu) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Ocupacion ocupacion = new Ocupacion();
		try {
			ocupacion=ocupacionServicio.listarId(idOcu);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Ocupacion>(ocupacion, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Ocupacion>(ocupacion, HttpStatus.OK);
	}
	@DeleteMapping( value = "/eliminar/{idOcu}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarOcupacionPorId(@PathVariable("idOcu") Integer idOcu) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			ocupacionServicio.eliminar(idOcu);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarOcupacion(@RequestBody Ocupacion ocu) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Ocupacion ocupacion = new Ocupacion(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno =0;
		try {
			retorno=ocupacionServicio.registrar(ocu); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	
	@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarOcupacion(@RequestBody Ocupacion ocu) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Ocupacion ocupacion = new Ocupacion(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno =0;
		try {
			retorno=ocupacionServicio.modificar(ocu); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Angular
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	
	@GetMapping( value = "/comprar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CompraDTO>> listarPreCompraTodas() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<CompraDTO> listaInfoEntradas = new ArrayList<>();
		try {
			listaInfoEntradas = ocupacionServicio.listarPrecompraEntrada();			
			
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<CompraDTO>>(listaInfoEntradas, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<CompraDTO>>(listaInfoEntradas, HttpStatus.OK);
	}
	
}
