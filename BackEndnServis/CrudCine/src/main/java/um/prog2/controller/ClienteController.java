package um.prog2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.prog2.model.Cliente;
import um.prog2.service.IClienteService;

@RestController 
@RequestMapping("/cliente") 
public class ClienteController {
	
	@Autowired
	private IClienteService clienteservicio;


	
	@GetMapping( value = "/listarPageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Cliente>> listarClientesPageable(Pageable pageable) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Page<Cliente> clientes = null;
		try {
			clientes=clienteservicio.listAllbyPage(pageable);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Page<Cliente>>(clientes, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Page<Cliente>>(clientes, HttpStatus.OK);
	}
	
	@GetMapping( value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Cliente>> listarClientesTodos() {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		List<Cliente> cliente = new ArrayList<>();
		try {
			cliente=clienteservicio.listar();
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<List<Cliente>>(cliente, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<List<Cliente>>(cliente, HttpStatus.OK);
	}
	
	
	@GetMapping( value = "/listar/{idCli}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> listarClientePorId(@PathVariable("idCli") Integer idCli) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		Cliente cliente = new Cliente();
		try {
			cliente=clienteservicio.listarId(idCli);
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Cliente>(cliente, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}
	
	@DeleteMapping( value = "/eliminar/{idCli}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminarEntradaPorId(@PathVariable("idCli") Integer idCli) {
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Calificacion calificacions = new Calificacion();
		int resultado=0;
		try {
			//calificacions=califServicio.eliminar(idCal);
			clienteservicio.eliminar(idCli);
			resultado =1;
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			//return new ResponseEntity<Calificacion>(calificacions, HttpStatus.INTERNAL_SERVER_ERROR);
			resultado=0;
		}	
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}
	@PostMapping( value = "/registrar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarCliente(@RequestBody Cliente cl) {// @RequestBody formatea a JSON
		System.out.println(cl.toString());
		System.out.println("sssssssssssssssssssssssssss");
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Cliente cliente = new Cliente(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=clienteservicio.registrar(cl); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	
	@PutMapping( value = "/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> modificarCliente(@RequestBody Cliente cli) {// @RequestBody formatea a JSON
		
		//El objeto existe si o si, pero ¿se usará y dará valores a sus atributos o termina vacio?
		//Cliente cliente = new Cliente(); //if(id)no hubo registro-> algo  else{si -> algo}
		int retorno=0;
		try {
			retorno=clienteservicio.modificar(cli); // retorna id 
		} catch (Exception e) {
			//Return: un paciente vacio e info "Internal_server_.." eso lo gestionaré en el backEnd con Anglar
			return new ResponseEntity<Integer>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<Integer>(retorno, HttpStatus.OK);
	}
	
		
}
