package um.prog2.dto;
import java.util.List;

import um.prog2.model.Cliente;
import um.prog2.model.Funcion;
import um.prog2.model.Butaca;

public class CompraDTO {
	
	//private Cliente cliente;
	private Butaca butacas;
	private Funcion funcion;
	public CompraDTO() {
		super();
	}
	/*
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}*/

	public Funcion getFuncion() {
		return funcion;
	}
	public Butaca getButacas() {
		return butacas;
	}

	public void setButacas(Butaca butacas) {
		this.butacas = butacas;
	}

	public void setFuncion(Funcion funcion) {
		this.funcion = funcion;
	}	
	
}
