package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="funcion")
public class Funcion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int funcion_id;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime fecha;
	
	//Muchas funciones para una sala
	@ManyToOne
	@JoinColumn(name="sala_id",nullable=false)
	private Sala sala;
	
	@ManyToOne
	@JoinColumn(name="pelicula_id",nullable=false)
	private Pelicula pelicula;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;

	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;

	public Funcion() {
		super();
	}

	public int getFuncion_id() {
		return funcion_id;
	}

	public void setFuncion_id(int funcion_id) {
		this.funcion_id = funcion_id;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

	public Pelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(Pelicula pelicula) {
		this.pelicula = pelicula;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdate() {
		return update;
	}

	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}
	
	
}
