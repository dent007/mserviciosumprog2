package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="pelicula")
public class Pelicula {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pelicula_id;
	
	@ManyToOne
	@JoinColumn(name="calificacion_id", nullable=false )
	private Calificacion calificacion;//Desde acá navego a Califición con JPQ
	
	@Column(name="titulo",nullable=false,length=50 )
	private String titulo;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime estreno;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;
	
	
	public Pelicula() {
		super();
	}
	public int getPelicula_id() {
		return pelicula_id;
	}
	public void setPelicula_id(int pelicula_id) {
		this.pelicula_id = pelicula_id;
	}
	public Calificacion getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Calificacion calificacion) {
		this.calificacion = calificacion;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public LocalDateTime getEstreno() {
		return estreno;
	}
	public void setEstreno(LocalDateTime estreno) {
		this.estreno = estreno;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public LocalDateTime getUpdate() {
		return update;
	}
	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}

	
}
