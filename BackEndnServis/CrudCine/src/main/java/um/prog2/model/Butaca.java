package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;


@Entity
@Table(name="butaca")
public class Butaca {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int butaca_id;
	
	@ManyToOne
	@JoinColumn(name="sala_id",nullable=false)
	private Sala sala;
	
	@Column(name="fila", nullable=false,length=1)
	private String  fila;
	
	@Column(name="numero")
	private int numero;
	
	@Column(name="descripcion", nullable=false)
	private String  descripcion;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;

	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;
	
	public Butaca() {
		super();
	}

	public int getButaca_id() {
		return butaca_id;
	}

	public void setButaca_id(int butaca_id) {
		this.butaca_id = butaca_id;
	}

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

	public String getFila() {
		return fila;
	}

	public void setFila(String fila) {
		this.fila = fila;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdate() {
		return update;
	}

	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}	
}







