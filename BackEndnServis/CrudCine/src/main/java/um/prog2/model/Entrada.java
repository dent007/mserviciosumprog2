package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="entrada")
public class Entrada {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int entrada_id;
	
	@Column(name="descripcion", nullable=false)
	private String descripcion;
	
	@Column(name="valor", nullable=false)
	private int valor;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;

	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime update;


	public Entrada() {
		super();
	}


	public int getEntrada_id() {
		return entrada_id;
	}


	public void setEntrada_id(int entrada_id) {
		this.entrada_id = entrada_id;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public int getValor() {
		return valor;
	}


	public void setValor(int valor) {
		this.valor = valor;
	}


	public LocalDateTime getCreated() {
		return created;
	}


	public void setCreated(LocalDateTime created) {
		this.created = created;
	}


	public LocalDateTime getUpdate() {
		return update;
	}


	public void setUpdate(LocalDateTime update) {
		this.update = update;
	}
	
		
}
