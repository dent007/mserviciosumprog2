package um.prog2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication


public class EurekaServerRegterApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerRegterApplication.class, args);
		System.out.println("EUREKA BIEN!!!");
	}

}
