package um.prog2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.prog2.model.Cliente;
import um.prog2.model.Tarjeta;
import um.prog2.model.Tarjeta.Tipo;

import java.lang.String;
//import java.util.List;
@Repository
public interface ITarjetaDAO extends JpaRepository<Tarjeta, Integer>{
	
//List<Tarjeta>findByNumeroAndCliente(String nm, Cliente c);	  
Tarjeta findByNumeroAndClienteAndTipo(String nm, Cliente c,Tipo tp);	
Tarjeta findByNumeroAndCliente(String nm, Cliente c);	 

}
