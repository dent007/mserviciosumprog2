package um.prog2.service;

import java.util.List;

import um.prog2.dto.DisponibilidadTarjetaPagoDTO;
import um.prog2.dto.PagoClienteDTO;
import um.prog2.dto.TarjetaSaldoDTO;
import um.prog2.model.Tarjeta;

public interface ITarjetaService {
	int registrar(Tarjeta tar);
	int modificar(Tarjeta tar);
	void eliminar(int idTar);
	Tarjeta listarId(int idTar) ;
	List<Tarjeta> listar(); 
	int pagarActalizarSaldo(PagoClienteDTO pagoClienteDTO);
	
	//DisponibilidadTarjetaPagoDTOTarjetaSaldoDTO preguntarSaldoTarjeta(PagoClienteDTO pagoClienteDTO);
	TarjetaSaldoDTO preguntarSaldoTarjeta(DisponibilidadTarjetaPagoDTO disponibilidadTarjetaPagoDTO);
}
