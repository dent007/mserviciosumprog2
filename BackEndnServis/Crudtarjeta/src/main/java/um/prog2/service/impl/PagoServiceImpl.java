package um.prog2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.prog2.dao.IPagoDAO;
import um.prog2.model.Pago;
import um.prog2.service.IPagoService;

 @Service
public class PagoServiceImpl implements IPagoService{
	
	@Autowired
	private IPagoDAO pagoDao;
	
	@Override
	public int registrar(Pago pag) {
		
		int salida=0;
		salida=pagoDao.save(pag)!=null ? pag.getPago_id():0;
		return salida >0 ? 1:0;
	}

	@Override
	public int modificar(Pago pag) {
		int salida=0;
		salida=pagoDao.save(pag)!=null ? pag.getPago_id():0;
		return salida >0 ? 1:0;
	}

	@Override
	public void eliminar(int idPag) {
		pagoDao.delete(idPag);
		
	}

	@Override
	public Pago listarId(int idPag) {
		// TODO Auto-generated method stub
		return pagoDao.findOne(idPag);
	}

	@Override
	public List<Pago> listar() {
		
		return pagoDao.findAll();
	}

}
