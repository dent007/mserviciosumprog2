package um.prog2.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="pago")
public class Pago {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int pago_id;
	
	@ManyToOne
	@JoinColumn(name="tarjeta_id", nullable=false )
	private Tarjeta tarjeta;
	
	@Column(name="importe", nullable=false)
	private int importe;
	
	@Column(name="pago_uuid", nullable=false)
	private String pago_uuid;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDateTime created;
	
	@JsonSerialize(using=ToStringSerializer.class)	
	private LocalDateTime updated;

	public Pago() {
		super();
	}

	public int getPago_id() {
		return pago_id;
	}

	public void setPago_id(int pago_id) {
		this.pago_id = pago_id;
	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	public int getImporte() {
		return importe;
	}

	public void setImporte(int importe) {
		this.importe = importe;
	}

	public String getPago_uuid() {
		return pago_uuid;
	}

	public void setPago_uuid(String pago_uuid) {
		this.pago_uuid = pago_uuid;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}
/*
	@Override
	public String toString() {
		return "Pago [pago_id=" + pago_id + ", tarjeta=" + tarjeta + ", importe=" + importe + ", pago_uuid=" + pago_uuid
				+ ", created=" + created + ", updated=" + updated + "]";
	}
*/
}
