package um.prog2.dto;

public class TarjetaSaldoDTO {
	private int idTarjeta;
	private int saldo;
	
	public TarjetaSaldoDTO() {
		super();
	}
	public int getIdTarjeta() {
		return idTarjeta;
	}
	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	public int getSaldo() {
		return saldo;
	}
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	@Override
	public String toString() {
		return "TarjetaSaldoDTO [idTarjeta=" + idTarjeta + ", saldo=" + saldo + "]";
	}
	

}
