package um.prog2.dto;

public class UserClienteDto {
	private String userLoing;
	private String rolLoin;
	
	public UserClienteDto() {
		super();
	}
	
	public UserClienteDto(String userLoing, String rolLoin) {
		super();
		this.userLoing = userLoing;
		this.rolLoin = rolLoin;
	}

	public String getUserLoing() {
		return userLoing;
	}
	public void setUserLoing(String userLoing) {
		this.userLoing = userLoing;
	}
	public String getRolLoin() {
		return rolLoin;
	}
	public void setRolLoin(String rolLoin) {
		this.rolLoin = rolLoin;
	}

	@Override
	public String toString() {
		return "UserClienteDto [userLoing=" + userLoing + ", rolLoin=" + rolLoin + "]";
	}
	

}
